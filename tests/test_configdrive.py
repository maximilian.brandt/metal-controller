import unittest
import unittest.mock
import uuid

from unittest.mock import sentinel

import ddt

import metal_controller.configdrive as configdrive


@ddt.ddt
class Test_common_filter_func(unittest.TestCase):
    @ddt.data(
        ".foo",
        ".bar",
        ".git",
        ".",
        "..",
        "foo/.bar"
    )
    def test_excludes_dotfiles(self, name):
        info = unittest.mock.Mock([])
        info.name = name
        info.mode = 0o600

        self.assertIsNone(configdrive._common_filter_func(info))

    @ddt.data(
        "foo",
        "bar",
        "git",
        "managed-k8s",
        "foo/bar"
    )
    def test_includes_other_files(self, name):
        info = unittest.mock.Mock([])
        info.name = name
        info.mode = 0o700

        self.assertIs(configdrive._common_filter_func(info), info)


class Test_collect_static_files(unittest.TestCase):
    @unittest.mock.patch("importlib.resources.files")
    def test_includes_all_directories_in_package_staticdir(
            self,
            files):
        common_prefix = uuid.uuid4()

        def mkentry(name):
            entry = unittest.mock.MagicMock(["__str__"])
            entry.name = name
            entry.__str__.return_value = f"{common_prefix}/{name}"
            return entry

        directory = [
            mkentry("path1"),
            mkentry("path2"),
            mkentry("path3"),
        ]

        subpath = unittest.mock.Mock(["iterdir"])
        subpath.iterdir.return_value = directory
        root_path = unittest.mock.MagicMock()
        root_path.__truediv__.return_value = subpath
        files.return_value = root_path

        tarfile = unittest.mock.Mock(["add"])

        configdrive._collect_static_files(tarfile)

        files.assert_called_once_with("metal_controller.configdrive")
        root_path.__truediv__.assert_called_once_with("static")

        self.assertCountEqual(
            [
                unittest.mock.call(
                    str(entry),
                    arcname=entry.name,
                    filter=configdrive._common_filter_func,
                )
                for entry in directory
            ],
            tarfile.add.mock_calls,
        )


class Test_collect_cluster_repo(unittest.TestCase):
    def test_adds_cluster_repo_with_common_filter(self):
        tarfile = unittest.mock.Mock(["add"])

        path = unittest.mock.MagicMock()
        path.__str__.return_value = "the fullpath"

        configdrive._collect_cluster_repo(
            tarfile,
            path,
        )

        tarfile.add.assert_called_once_with(
            "the fullpath",
            arcname="cluster",
            filter=configdrive._common_filter_func,
        )


class Testbuild_tarball(unittest.TestCase):
    @unittest.mock.patch("base64.b64encode")
    @unittest.mock.patch("metal_controller.configdrive._collect_static_files")
    @unittest.mock.patch("metal_controller.configdrive._collect_cluster_repo")
    @unittest.mock.patch("tarfile.open")
    @unittest.mock.patch("io.BytesIO")
    def test_collects_tarball_in_memory(
            self,
            BytesIO,
            tarfile_open,
            collect_cluster_repo,
            collect_static_files,
            b64encode):
        buf = unittest.mock.Mock(["getvalue"])
        tarfile = unittest.mock.Mock([])
        tarfile_open.return_value.__enter__.return_value = tarfile
        BytesIO.return_value = buf
        b64encode.return_value = sentinel.encoded

        result = configdrive.build_tarball(
            cluster_repository=sentinel.cluster_repo,
        )

        BytesIO.assert_called_once_with()

        tarfile_open.assert_called_once_with(
            fileobj=buf,
            mode="w:bz2",
        )

        collect_cluster_repo.assert_called_once_with(
            tarfile,
            sentinel.cluster_repo,
        )

        collect_static_files.assert_called_once_with(tarfile)

        buf.getvalue.assert_called_once_with()

        b64encode.assert_called_once_with(buf.getvalue())

        self.assertEqual(result, sentinel.encoded)


class Testassemble_deploy_script(unittest.TestCase):
    @unittest.mock.patch("importlib.resources.files")
    def test_collects_tarball_in_memory(self, files):
        deploy_script = b"foobar\n{{ tarball }}\nbaz\n"

        deploy_script_path = unittest.mock.Mock(["read_bytes"])
        deploy_script_path.read_bytes.return_value = deploy_script
        templates_path = unittest.mock.MagicMock()
        templates_path.__truediv__.return_value = deploy_script_path
        root_path = unittest.mock.MagicMock()
        root_path.__truediv__.return_value = templates_path

        files.return_value = root_path

        result = configdrive.assemble_deploy_script(
            b"the tarball",
        )

        files.assert_called_once_with("metal_controller.configdrive")
        root_path.__truediv__.assert_called_once_with("templates")
        templates_path.__truediv__.assert_called_once_with("deploy.sh")
        deploy_script_path.read_bytes.assert_called_once_with()

        self.assertEqual(
            b"foobar\nthe tarball\nbaz\n",
            result,
        )


class Test_pack_mime(unittest.TestCase):
    @unittest.mock.patch("email.mime.base.MIMEBase")
    def test_assembles_text_data(self, MIMEBase):
        part = unittest.mock.Mock()
        MIMEBase.return_value = part

        result = configdrive._pack_mime(
            sentinel.name,
            sentinel.maintype,
            sentinel.subtype,
            "text data",
        )

        MIMEBase.assert_called_once_with(sentinel.maintype, sentinel.subtype)
        part.set_payload.assert_called_once_with("text data", charset="utf-8")
        part.add_header.assert_called_once_with(
            "Content-Disposition",
            "attachment",
        )
        part.set_param.assert_called_once_with(
            "filename", sentinel.name,
            header="Content-Disposition",
        )

        self.assertEqual(result, part)

    @unittest.mock.patch("email.mime.base.MIMEBase")
    def test_assembles_binary_data(self, MIMEBase):
        part = unittest.mock.Mock()
        MIMEBase.return_value = part

        result = configdrive._pack_mime(
            sentinel.name,
            sentinel.maintype,
            sentinel.subtype,
            b"binary data",
        )

        MIMEBase.assert_called_once_with(sentinel.maintype, sentinel.subtype)
        part.set_payload.assert_called_once_with(b"binary data")
        part.add_header.assert_called_once_with(
            "Content-Disposition",
            "attachment",
        )
        part.set_param.assert_called_once_with(
            "filename", sentinel.name,
            header="Content-Disposition",
        )

        self.assertEqual(result, part)
