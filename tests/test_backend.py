import contextlib
import unittest
import unittest.mock
from unittest.mock import sentinel

import metal_controller.backend as backend
import metal_controller.model as model


class TestBackend(unittest.TestCase):
    def test_ipa_config_context(self):
        conn = unittest.mock.Mock()
        ironic_node = unittest.mock.Mock()
        ironic_node.id = sentinel.ironic_id

        action = backend.ManageAction(
            backend_node_id=sentinel.backend_node_id,
            driver="redfish",
            driver_info={},
            name=sentinel.name,
            config_context={
                "deploy_kernel": sentinel.deploy_kernel,
                "deploy_ramdisk": sentinel.deploy_ramdisk
            },
            description=sentinel.description,
            metal_controller_deployment_mode=model.MetalControllerDeployMethod
            .YAOOK_K8S
        )

        ironic_node.properties = {}

        with contextlib.ExitStack() as stack:
            update_extra_dict = stack.enter_context(
                unittest.mock.patch.object(
                    model, "update_extra_dict",
                )
            )
            update_extra_dict.return_value = sentinel.extra_dict

            action.apply_to(conn=conn, ironic_node=ironic_node)

        conn.baremetal.update_node.assert_called_with(
            sentinel.ironic_id,
            driver='redfish',
            driver_info={
                'deploy_kernel': sentinel.deploy_kernel,
                'deploy_ramdisk': sentinel.deploy_ramdisk
            },
            extra=sentinel.extra_dict,
            deploy_interface='direct',
            properties={'capabilities': 'boot_mode:uefi'},
            name=sentinel.name,
            description=sentinel.description,
            boot_interface='ipxe',
            power_interface='redfish',
            management_interface='redfish',
            vendor_interface='redfish'
        )
