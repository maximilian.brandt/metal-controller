#!/bin/bash
set -euo pipefail
common_path_prefix="yaook"
cluster="$1"
cluster_path="$common_path_prefix/$cluster"

vault secrets enable -path="$cluster_path/kv" -version=2 kv
vault secrets enable -path="$cluster_path/ssh-ca" ssh
vault write "$cluster_path/ssh-ca/config/ca" generate_signing_key=true
