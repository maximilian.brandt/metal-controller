#!/usr/bin/python3
import math
import uuid
import random

rng = random.SystemRandom()

# omitting c to avoid ambiguity with k
# omitting v to avoid ambiguity with f
# omitting w to avoid ambiguity with v for german speakers :)
# omitting g to avoid ambiguity with k
# omitting p to avoid ambiguity with b
#  (preferred to remove p to avoid common bad words™)
# omitting z to avoid ambiguity with s
# omitting q to avoid ambiguity with k
# omitting y to avoid ambiguity with j
CONSONANTS = "bdfhjklmnrstx"
VOWELS = "aeiou"

K8S = "".join(map(chr, range(ord("a"), ord("z")+1))) + "0123456789"


DOCKER_ADJECTIVES = [
    "admiring",
    "adoring",
    "affectionate",
    "agitated",
    "amazing",
    "angry",
    "awesome",
    "beautiful",
    "blissful",
    "bold",
    "boring",
    "brave",
    "busy",
    "charming",
    "clever",
    "cool",
    "compassionate",
    "competent",
    "condescending",
    "confident",
    "cranky",
    "crazy",
    "dazzling",
    "determined",
    "distracted",
    "dreamy",
    "eager",
    "ecstatic",
    "elastic",
    "elated",
    "elegant",
    "eloquent",
    "epic",
    "exciting",
    "fervent",
    "festive",
    "flamboyant",
    "focused",
    "friendly",
    "frosty",
    "funny",
    "gallant",
    "gifted",
    "goofy",
    "gracious",
    "great",
    "happy",
    "hardcore",
    "heuristic",
    "hopeful",
    "hungry",
    "infallible",
    "inspiring",
    "interesting",
    "intelligent",
    "jolly",
    "jovial",
    "keen",
    "kind",
    "laughing",
    "loving",
    "lucid",
    "magical",
    "mystifying",
    "modest",
    "musing",
    "naughty",
    "nervous",
    "nice",
    "nifty",
    "nostalgic",
    "objective",
    "optimistic",
    "peaceful",
    "pedantic",
    "pensive",
    "practical",
    "priceless",
    "quirky",
    "quizzical",
    "recursing",
    "relaxed",
    "reverent",
    "romantic",
    "sad",
    "serene",
    "sharp",
    "silly",
    "sleepy",
    "stoic",
    "strange",
    "stupefied",
    "suspicious",
    "sweet",
    "tender",
    "thirsty",
    "trusting",
    "unruffled",
    "upbeat",
    "vibrant",
    "vigilant",
    "vigorous",
    "wizardly",
    "wonderful",
    "xenodochial",
    "youthful",
    "zealous",
    "zen",
]


DOCKER_NAMES = [
    "agnesi",
    "albattani",
    "allen",
    "almeida",
    "antonelli",
    "archimedes",
    "ardinghelli",
    "aryabhata",
    "austin",
    "babbage",
    "banach",
    "banzai",
    "bardeen",
    "bartik",
    "bassi",
    "beaver",
    "bell",
    "benz",
    "bhabha",
    "bhaskara",
    "black",
    "blackburn",
    "blackwell",
    "bohr",
    "booth",
    "borg",
    "bose",
    "bouman",
    "boyd",
    "brahmagupta",
    "brattain",
    "brown",
    "buck",
    "burnell",
    "cannon",
    "carson",
    "cartwright",
    "carver",
    "cerf",
    "chandrasekhar",
    "chaplygin",
    "chatelet",
    "chatterjee",
    "chaum",
    "chebyshev",
    "clarke",
    "cohen",
    "colden",
    "cori",
    "cray",
    "curran",
    "curie",
    "darwin",
    "davinci",
    "dewdney",
    "dhawan",
    "diffie",
    "dijkstra",
    "dirac",
    "driscoll",
    "dubinsky",
    "easley",
    "edison",
    "einstein",
    "elbakyan",
    "elgamal",
    "elion",
    "ellis",
    "engelbart",
    "euclid",
    "euler",
    "faraday",
    "feistel",
    "fermat",
    "fermi",
    "feynman",
    "franklin",
    "gagarin",
    "galileo",
    "galois",
    "ganguly",
    "gates",
    "gauss",
    "germain",
    "goldberg",
    "goldstine",
    "goldwasser",
    "golick",
    "goodall",
    "gould",
    "greider",
    "grothendieck",
    "haibt",
    "hamilton",
    "haslett",
    "hawking",
    "hellman",
    "heisenberg",
    "hermann",
    "herschel",
    "hertz",
    "heyrovsky",
    "hodgkin",
    "hofstadter",
    "hoover",
    "hopper",
    "hugle",
    "hypatia",
    "ishizaka",
    "jackson",
    "jang",
    "jemison",
    "jennings",
    "jepsen",
    "johnson",
    "joliot",
    "jones",
    "kalam",
    "kapitsa",
    "kare",
    "keldysh",
    "keller",
    "kepler",
    "khayyam",
    "khorana",
    "kilby",
    "kirch",
    "knuth",
    "kowalevski",
    "lalande",
    "lamarr",
    "lamport",
    "leakey",
    "leavitt",
    "lederberg",
    "lehmann",
    "lewin",
    "lichterman",
    "liskov",
    "lovelace",
    "lumiere",
    "mahavira",
    "margulis",
    "matsumoto",
    "maxwell",
    "mayer",
    "mccarthy",
    "mcclintock",
    "mclaren",
    "mclean",
    "mcnulty",
    "mendel",
    "mendeleev",
    "meitner",
    "meninsky",
    "merkle",
    "mestorf",
    "mirzakhani",
    "montalcini",
    "moore",
    "morse",
    "murdock",
    "moser",
    "napier",
    "nash",
    "neumann",
    "newton",
    "nightingale",
    "nobel",
    "noether",
    "northcutt",
    "noyce",
    "panini",
    "pare",
    "pascal",
    "pasteur",
    "payne",
    "perlman",
    "pike",
    "poincare",
    "poitras",
    "proskuriakova",
    "ptolemy",
    "raman",
    "ramanujan",
    "ride",
    "ritchie",
    "rhodes",
    "robinson",
    "roentgen",
    "rosalind",
    "rubin",
    "saha",
    "sammet",
    "sanderson",
    "satoshi",
    "shamir",
    "shannon",
    "shaw",
    "shirley",
    "shockley",
    "shtern",
    "sinoussi",
    "snyder",
    "solomon",
    "spence",
    "stonebraker",
    "sutherland",
    "swanson",
    "swartz",
    "swirles",
    "taussig",
    "tereshkova",
    "tesla",
    "tharp",
    "thompson",
    "torvalds",
    "tu",
    "turing",
    "varahamihira",
    "vaughan",
    "villani",
    "visvesvaraya",
    "volhard",
    "wescoff",
    "wilbur",
    "wiles",
    "williams",
    "williamson",
    "wilson",
    "wing",
    "wozniak",
    "wright",
    "wu",
    "yalow",
    "yonath",
    "zhukovsky",
]


def gen_uuid(prefix):
    return f"{prefix}-{uuid.uuid4()}"


def gen_k8s(prefix):
    suffix = "".join(
        rng.choice(K8S)
        for _ in range(5)
    )
    return f"{prefix}-{suffix}"


def gen_docker(prefix):
    return f"{prefix}-{rng.choice(DOCKER_ADJECTIVES)}-{rng.choice(DOCKER_NAMES)}"  # noqa:E501


def gen_cvc_Nnnn(prefix):
    num = rng.randint(1000, 9999)
    cvc = rng.choice(CONSONANTS) + rng.choice(VOWELS) + rng.choice(CONSONANTS)
    return f"{prefix}-{cvc}-{num}"


def gen_cvcv_Nnnn(prefix):
    num = rng.randint(1000, 9999)
    cvcv = (
        rng.choice(CONSONANTS) + rng.choice(VOWELS) +
        rng.choice(CONSONANTS) + rng.choice(VOWELS)
    )
    return f"{prefix}-{cvcv}-{num}"


def gen_cvcv_nnnn(prefix):
    num = rng.randint(0, 9999)
    cvcv = (
        rng.choice(CONSONANTS) + rng.choice(VOWELS) +
        rng.choice(CONSONANTS) + rng.choice(VOWELS)
    )
    return f"{prefix}-{cvcv}-{num:04d}"


def pretty_number(n):
    prefixes = [
        "",
        "k",
        "M",
        "G",
        "T",
        "P",
        "E",
        "Z",
        "Y"
    ]
    scale = math.floor(math.log(n, 1000))
    try:
        prefix = prefixes[scale]
    except IndexError:
        # oh my
        factor = 1000**scale
        return "{:.1f}e{}".format(n / factor, scale*3)

    if not prefix:
        return "{:.0f}".format(n)

    factor = 1000**scale
    return "{:.1f}{}".format(n / factor, prefix)


SAMPLE_PREFIXES = ["mon", "osd", "cmp", "ctl", "gen"]


def debug_space(name, noptions, gen):
    samples = [
        gen(prefix)
        for prefix in SAMPLE_PREFIXES
        for _ in range(2)
    ]

    print(name)
    print(f"  entropy               : {math.log(noptions, 2):.2f} bits")
    print(f"  number of unique names: {pretty_number(noptions)}")
    print(f"  collision threshold   : {pretty_number(math.sqrt(noptions))}")
    print(f"   examples (using sample prefixes {', '.join(SAMPLE_PREFIXES)}):")
    print("     ", "\n      ".join(samples))


debug_space("uuid4", 2**122, gen_uuid)
debug_space("k8s", len(K8S)**5, gen_k8s)
debug_space("docker", len(DOCKER_ADJECTIVES)*len(DOCKER_NAMES),
            gen_docker)
debug_space("cvc-Nnnn", len(CONSONANTS)**2*len(VOWELS)*9*10**3, gen_cvc_Nnnn)
debug_space("cvcv-Nnnn", len(CONSONANTS)**2*len(VOWELS)**2*9*10**3,
            gen_cvcv_Nnnn)
debug_space("cvcv-nnnn", len(CONSONANTS)**2*len(VOWELS)**2*10**4,
            gen_cvcv_nnnn)
